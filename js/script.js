(function ($) {
	$(document).ready(function() {	

		//EX-1
		/*$('a').on('click', function(event) {
			event.preventDefault();
			console.log($(this).attr('href'));
		});*/

		//EX-2'

		/*$('body').mousemove(function(event) {
			console.log(event.pageX + ", " + event.pageY);
			$('.bodyEx2 div').css({
				marginLeft: event.pageX,
				marginTop: event.pageY
			});
		});*/

		//EX-3

		$('a[href="#1"]').on('click', function() {
			
			$('body').animate({scrollTop: 40}, 1000);
		});

		$('a[href="#2"]').on('click', function() {
			
			$('body').animate({scrollTop: 1040}, 1000);
		});

		$('a[href="#3"]').on('click', function() {
			
			$('body').animate({scrollTop: 2080}, 1000);
		});

		$('a[href="#4"]').on('click', function() {
			
			$('body').animate({scrollTop: 3120}, 1000);
		});

		$('a[href="#top"]').on('click', function() {
			
			$('body').animate({scrollTop: 0}, 1000);
		});

	});
})
(jQuery);